*** Settings ***

Documentation       Documentação da API: https://controle-estoque-backend.herokuapp.com/api-docs/#/Provider/post_providers
Library             RequestsLibrary
Library             Collections
Library             FakerLibrary

*** Variable ***
${URL_API}          https://controle-estoque-backend.herokuapp.com/api/v1/ 
${NAME_PROVISORY}


*** Keywords ***

Conectar a minha API
    Create Session      Medicamentos     ${URL_API}    verify=true


Dado que é realizado uma busca pelos Providers
    ${RESPOSTA}         GET On Session    Medicamentos     providers     
    Log                 ${RESPOSTA.text} 
    Set Test Variable   ${RESPOSTA} 

Dado que é realizado uma busca pelo provider "${PROVIDER__ID}"
    ${RESPOSTA}         GET On Session    Medicamentos     providers/${PROVIDER__ID}     
    Log                 ${RESPOSTA.text} 
    Set Test Variable   ${RESPOSTA} 

Então deve exibir o status code 
    [Arguments]                 ${STATUSCODE_DESEJADO}
    Should Be Equal As Strings   ${RESPOSTA.status_code}     ${STATUSCODE_DESEJADO}

E o reasion deve ser 
    [Arguments]                 ${REASON_DESEJADO}
    Should Be Equal As Strings   ${RESPOSTA.reason}          ${REASON_DESEJADO}

Dado que é realizado um cadastrado de um proivder
    ${NOMEFAKE}         FakerLibrary.Name
    ${HEADERS}          Create Dictionary      content-type=application/json
    ${RESPOSTA}         Post On Session    Medicamentos     providers
    ...                 data={"name": "${NOMEFAKE}","cnpj": ""}
    ...                 headers=${HEADERS}
    Log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}


Quando é informado o mesmo parametros para cadastro  
    ${NAME_PROVISORY}   Set Variable    ${RESPOSTA.json()}[name]
    ${HEADERS}          Create Dictionary      content-type=application/json
    ${RESPOSTA}         Post On Session    Medicamentos     providers
    ...                 data={"name": "${NAME_PROVISORY}","cnpj": ""}
    ...                 headers=${HEADERS}
    Log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}


Edeve é exibido a mensagem: 
    [Arguments]                 ${MESSAGE_DESEJADO}
    Should Be Equal As Strings  ${MESSAGE_DESEJADO}          ${RESPOSTA.json()}[message]