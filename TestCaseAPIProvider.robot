*** Settings ***

Documentation       Documentação da API: https://controle-estoque-backend.herokuapp.com/api-docs/#/Provider/post_providers
Resource            ./resources/ResourceAPI.robot
Suite Setup         Conectar a minha API

***Test Cases***
Cenário: Cadastrando um provider
    Dado que é realizado um cadastrado de um proivder
    Então deve exibir o status code     201
    E o reasion deve ser    Created

Cenário: Cadastrando um provider com o mesmo nome
    Dado que é realizado um cadastrado de um proivder
    Quando é informado o mesmo parametros para cadastro  
    Edeve é exibido a mensagem:     supplier already registered
    Então deve exibir o status code     400


Cenário: retornar todos os Providers com sucesso
    Dado que é realizado uma busca pelos Providers
    Então deve exibir o status code     200
    E o reasion deve ser    OK

Cenário: Buscar um provider específico
    Dado que é realizado uma busca pelo provider "7160bfb9-1062-458e-8234-66f0b5f751af"
    Então deve exibir o status code     200
    E o reasion deve ser    OK



